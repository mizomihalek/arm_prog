/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

#define HTU21D_ADDRESS 							(0x40)  // a p�ra m�ro c�me
#define SOFT_RESET                   0xFE
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HTU21D_Soft_Reset(void);
void HTU21D_parameres(void);
void HTU21D_homeres(void);
float procHumidityValue(uint16_t ValueTemp);
float procTemperatureValue(uint16_t ValueTemp);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

	float humidity = 0u;
	float temperature = 0u;
	
	uint8_t received_para[2];
	uint8_t received_ho[2];
	
	uint8_t state_para=0;
	uint8_t state_ho=0;
	
	uint16_t get_humidity;
	uint16_t get_temperature;
	
	uint8_t timer_humortemp=0;
	
	void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
		if(htim->Instance==htim1.Instance){
					//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
					//HTU21D_parameres();
						HTU21D_homeres();
		}
		if(htim->Instance==htim2.Instance){
				//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
					HTU21D_parameres();
				//HTU21D_homeres();
		}
}
	
/*	
	void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim->Instance==htim1.Instance){
				//HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
				//HTU21D_parameres();
				HTU21D_homeres();
		}
	if(htim->Instance==htim2.Instance){
		  HTU21D_homeres();
		}*/
	

/*void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance==hi2c1.Instance)
	{
		get_humidity = (received_data[0]<<8) | received_data[1];
		get_humidity &= 0xFFFC;
		//return_value = (received_data[0]<<8) | received_data[1];
	}
}*/
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();

  /* USER CODE BEGIN 2 */
//	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
//	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 1);
	
	HTU21D_Soft_Reset();
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_TIM_Base_Start_IT(&htim2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	/*HTU21D_parameres();
	HAL_Delay(500);*/
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HTU21D_Soft_Reset(void){
	uint8_t reg_command[1];
  reg_command[0] = SOFT_RESET;
	HAL_I2C_Master_Transmit(&hi2c1, HTU21D_ADDRESS<<1, reg_command , sizeof(reg_command), 1000 );
}

void HTU21D_parameres(void){
	if(state_para) return;
	state_para = 1;
	uint8_t transmit[1] = {0xE5};

	HAL_I2C_Master_Transmit(&hi2c1, HTU21D_ADDRESS<<1, transmit, sizeof(transmit), 1000 );

	//HAL_I2C_Mem_Read_DMA(&hi2c1,HTU21D_ADDRESS<<1,0,sizeof(received_data),received_data, 1);
	//HAL_I2C_Master_Receive(&hi2c1, HTU21D_ADDRESS<<1, received_data, 2, 1000);
	//return_value = (received_data[0]<<8) | received_data[1];
	HAL_I2C_Master_Receive_IT(&hi2c1,HTU21D_ADDRESS<<1,received_para, 16);
	get_humidity = (received_para[0]<<8) | received_para[1];
	get_humidity &= 0xFFF0;
	if(get_humidity > 0) humidity = procHumidityValue(get_humidity);
	state_para = 0;
}

void HTU21D_homeres(void){
	if(state_ho) return;
	state_ho = 1;
	uint8_t transmit[1] = {0xE3};
	HAL_I2C_Master_Transmit(&hi2c1, HTU21D_ADDRESS<<1, transmit, sizeof(transmit), 1000 );
	HAL_I2C_Master_Receive_IT(&hi2c1,HTU21D_ADDRESS<<1,received_ho, 16);
	get_temperature = (received_ho[0]<<8) | received_ho[1];
	get_temperature &= 0xFFFC;
	if(get_temperature > 0) temperature = procTemperatureValue(get_temperature);
	state_ho = 0;
}

float procHumidityValue(uint16_t ValueTemp){
  float calc;
  calc = -6.0 + 125.0 * ValueTemp / 65535.0;
  //printf("paratartalom: %4.2f RH\n\r", calc );
	return calc;
}

float procTemperatureValue(uint16_t ValueTemp){
  float calc;
  calc = -46.85 + 175.72 * ValueTemp / 65535.0;
  //printf("\t\t\thomerseklet: %4.2f C\n\r", calc );
	return calc;
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
